module.exports = {
    devtool: 'source-map',
    entry: {
        entry: './src/index.js',
    },
    mode: 'development',
    module: {
        rules: [{
            exclude: /node_modules/,
            use: [{
                loader: 'babel-loader'
            }, {
                loader: 'prettier-loader'
            }],
            test: /\.js$/
        }]
    },
    output: {
        path: __dirname + '/dist',
        filename: 'bundle.js'
    }
}