// Ciao Andrea, tento di commentarti il codice, almeno hai un'idea di come lavoro.
// I import the chart from chart.js, located in node_modules
import Chart from "chart.js/auto";
// Get the context from the canvas in the HTML.
const ctx = document.getElementById("context");

// Create the websocket in which I operate.
export const socket = new WebSocket("ws://localhost:9000");

// Take the data from the socket.
// Everytime I receive data, the function operates
socket.addEventListener("message", (e) => {
  // Get the data and use JSON.parse to create an object.
  // HAVE TO USE THE .data PROPERTY OR I GET AN ERROR. (Javascript.info/websocket).
  // I read 2 values in the console, each updates every second (1000ms).
  console.log(JSON.parse(e.data));

  // Create an interval that, every second, pushes the data in the arrays.
  let parsedData = JSON.parse(e.data);
  labels.push(parsedData.time);
  data.push(Number(parsedData.value));
  exercise.update();
});

// Array creation.
const labels = [];
const data = [];

// Chart creation.
const exercise = new Chart(ctx, {
  type: "line",
  data: {
    labels,
    datasets: [
      {
        label: "My Data",
        data,
        fill: false,
        // Black chart for daltonism.
        borderColor: "rgb(0, 0, 0)",
        tension: 0.1,
      },
    ],
  },
  options: {
    scales: {
      y: {
        beginAtZero: true,
      },
    },
  },
});
